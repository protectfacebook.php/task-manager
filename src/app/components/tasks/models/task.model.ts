export  interface ITask{
  _id?: string;
  name?: string;
  description?: string;
  status?: boolean;
}

export class Task implements ITask{
  constructor(
    public _id?: string,
    public name?: string,
    public description?: string,
    public status?: boolean
  ){}
}



