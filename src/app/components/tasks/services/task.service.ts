import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { ITask } from '../models/task.model';

@Injectable({
  providedIn: 'root'
})
export class TaskService {

  private URL_SERVER: string = environment.urlApi + '/tasks'

  constructor(
    private http: HttpClient
  ) { }

  getAll(): Promise<ITask[]>{
    return this.http.get<ITask[]>(`${this.URL_SERVER}/get-all`).toPromise()
  }

  saveTask(data: ITask): Promise<ITask>{
    return this.http.post<ITask>(`${this.URL_SERVER}/save`, data).toPromise()
  }

}
