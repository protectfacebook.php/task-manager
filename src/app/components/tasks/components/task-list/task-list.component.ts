import { Component, OnInit } from '@angular/core';
import { ITask } from '../../models/task.model';
import { TaskService } from '../../services/task.service';

@Component({
  selector: 'app-task-list',
  templateUrl: './task-list.component.html',
  styleUrls: ['./task-list.component.css']
})
export class TaskListComponent implements OnInit {

  taskList: ITask[] = []

  constructor(
    private taskService: TaskService
  ) { }

  ngOnInit(): void {
    this.loadTasks()
  }

  loadTasks(): void{
    this.taskService.getAll()
      .then(res => {
        this.taskList  = res
      })
      .catch(error => {
        console.log(error)
      })
      .finally(() => {
        console.log("Finalizó")
      })
  }

}
