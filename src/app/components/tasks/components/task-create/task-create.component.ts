import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { TaskService } from '../../services/task.service';

@Component({
  selector: 'app-task-create',
  templateUrl: './task-create.component.html',
  styleUrls: ['./task-create.component.css'],
  providers: [FormBuilder],
})
export class TaskCreateComponent implements OnInit {

  fieldForFull?: any;

  createTaskForm: FormGroup = this.fb.group({
    name: [
      '',
      Validators.compose([
        Validators.required,
        Validators.minLength(2),
        Validators.maxLength(25),
      ]),
    ],
    description: '',
    status: '',
  });

  constructor(private fb: FormBuilder, private taskService: TaskService) {}

  ngOnInit(): void {}

  saveTask(): void {
    if (this.createTaskForm.invalid) {

    }else{
      this.taskService
        .saveTask(this.createTaskForm.value)
        .then((res) => {
          console.log(res);
        })
        .catch((error) => {
          console.log(error);
        });
    }
  }
}
